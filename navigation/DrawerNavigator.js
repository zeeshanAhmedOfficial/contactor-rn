import React from 'react';
import {Platform,Dimensions} from 'react-native';
import { createDrawerNavigator, createStackNavigator, createAppContainer } from 'react-navigation';
import Home from './../src/components/Home';
import Listing from './../src/components/Users/Listing';
import SignUp from './../src/components/Auth/SignUp';
import LogIn from './../src/components/Auth/LogIn';

const WIDTH = Dimensions.get('window').width;
const DrawerConfig = {
    drawerWidth : WIDTH*0.83
};
 
// const DrawerNavigator = createDrawerNavigator(
//     {
//         Home : { 
//             screen: Home 
//         }
//     },
//     DrawerConfig
// );

const DrawerNavigator = createStackNavigator(
	{
		SignUp : {screen: SignUp},
		Login : {screen: LogIn},
	  	Home: {screen: Home},
	  	Listing: {screen: Listing},
	},
	{
		defaultNavigationOptions: {
        	headerTitleStyle: { color: '#20B2AA', flex: 1, textAlign: 'center'},
		}
	}
);


export default createAppContainer(DrawerNavigator);