import React, {Component} from 'react';
import {View,Text,StyleSheet,Button,Alert,ActivityIndicator} from 'react-native';
import Listing from './Users/Listing';

export default class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true
        };
    }

    static navigationOptions = {
        title: 'HOME',
        headerLeft: null,
    };

    render() {
        const {navigate} = this.props.navigation;

        return (
            <View style={styles.container}>

                <View style={styles.textView}>
                    <Text style={styles.header}>Welcome to Contactor!</Text>
                    <Text style={styles.desc}>This app let you save your contacts. Contacts will be available anywhere in the world through this app.</Text>
                </View>

                <View style={styles.btnView}>
                    <View style={styles.btnBottomNav}>
                        <Button
                            onPress={ () => navigate('Home', {name: 'Jane'}) }
                            title="Home"
                            color="#000000"
                        />
                    </View>
                    <View style={styles.btnBottomNav}>
                        <Button
                            onPress={ 
                                () => navigate('Listing', {
                                    name: 'Users Listing'
                                })
                            }
                            title="Users"
                            color="#000000"
                        />
                    </View>
                    <View style={styles.btnBottomNav}>
                        <Button
                            onPress={ 
                                () => navigate('SignUp')
                            }
                            title="Logout"
                            color="#000000"
                        />
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container : {
        flex : 1,
    },
    textView : {
        flex : 1,
        justifyContent : 'center',
        alignItems: 'center',
        backgroundColor: '#20B2AA'
    },
    header : {
        color: 'white',
        fontSize : 28,
        fontWeight: 'bold',
    },
    desc : {
        paddingLeft: 20,
        paddingRight: 20,
        color: '#FFF8DC',
        fontSize : 18,
        textAlign: 'center',
        marginTop: 10,
    },
    btnView : {
        backgroundColor : "#893838",
        flexDirection : 'row',
        justifyContent: 'space-between',
        alignItems: 'stretch',
        alignContent : 'stretch',
    },
    btnBottomNav : {
        flex: 1,
        borderWidth: 1,
        borderColor: '#c0c0c0',
    },
});