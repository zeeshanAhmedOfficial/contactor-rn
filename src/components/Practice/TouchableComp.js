import React, { Component } from 'react';
import {
    View, StyleSheet, Alert, Platform, Text, TouchableHighlight, TouchableOpacity, TouchableNativeFeedback, TouchableWithoutFeedback, ScrollView
} from 'react-native';

export default class TouchableComp extends Component {

    _onPressBtn() {
        Alert.alert('You tapped on a button');
    }

    _onLongPressBtn() {
        Alert.alert('You long-press the button');
    }

    render() {
        return (
                <ScrollView>
                    <TouchableHighlight onPress={this._onPressBtn} underlayColor="white">
                        <View style={styles.button}>
                            <Text style={styles.buttonText}>TouchableHighlight</Text>
                        </View>
                    </TouchableHighlight>

                    <TouchableOpacity onPress={this._onPressBtn}>
                        <View style={styles.button}>
                            <Text style={styles.buttonText}>TouchableOpacity</Text>
                        </View>
                    </TouchableOpacity>

                    <TouchableNativeFeedback 
                        onPress={this._onPressBtn}
                        background={Platform.OS === 'android' ? TouchableNativeFeedback.SelectableBackground() : ''}>
                        <View style={styles.button}>
                            <Text style={styles.buttonText}>TouchableNativeFeedback</Text>
                        </View>
                    </TouchableNativeFeedback>

                    <TouchableWithoutFeedback onPress={this._onPressBtn}>
                        <View style={styles.button}>
                            <Text style={styles.buttonText}>TouchableWithoutFeedback</Text>
                        </View>
                    </TouchableWithoutFeedback>

                    <TouchableHighlight onLongPress={this._onLongPressBtn} underlayColor="white">
                        <View style={styles.button}>
                            <Text style={styles.buttonText}>Touchable with Long Press</Text>
                        </View>
                    </TouchableHighlight>

                    <TouchableNativeFeedback 
                        onPress={this._onPressBtn}
                        background={Platform.OS === 'android' ? TouchableNativeFeedback.SelectableBackground() : ''}>
                        <View style={styles.button}>
                            <Text style={styles.buttonText}>TouchableNativeFeedback</Text>
                        </View>
                    </TouchableNativeFeedback>

                    <TouchableWithoutFeedback onPress={this._onPressBtn}>
                        <View style={styles.button}>
                            <Text style={styles.buttonText}>TouchableWithoutFeedback</Text>
                        </View>
                    </TouchableWithoutFeedback>

                    <TouchableHighlight onLongPress={this._onLongPressBtn} underlayColor="white">
                        <View style={styles.button}>
                            <Text style={styles.buttonText}>Touchable with Long Press</Text>
                        </View>
                    </TouchableHighlight>
                </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    button : {
        marginBottom : 30,
        width: 250,
        alignItems : 'center',
        backgroundColor: '#2196F3'
    },

    buttonText : {
        padding: 20,
        color: 'white'
    }
});