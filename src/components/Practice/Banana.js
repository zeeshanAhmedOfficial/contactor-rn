import React, { Component } from 'react';
import { View, Image, Text, StyleSheet } from 'react-native';

export default class Banana extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            isShowingText : true,
        };

        // Toggle the state every second
        setInterval(() => (
            this.setState(prevState => (
                { isShowingText : !prevState.isShowingText }
            ))
        ), 500);
    }

    render() {
        return (
            <Text style={ [styles.text, this.props.stile] }>
                {this.props.name}
            </Text>
        );
    }
}

const styles = StyleSheet.create({
    text: {
        fontSize : 16,
        color: 'white',
    }
});