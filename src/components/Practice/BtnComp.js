import React, { Component } from 'react';
import {
    View,
    Alert,
    StyleSheet, 
    Button
} from 'react-native';

export default class BtnComp extends Component {

    _onPressBtn() {
        Alert.alert('You just tapped on me');
    }

    render() {
        return (
            <View>
                <View style={styles.buttonContainer}>
                    <Button 
                        onPress={ this._onPressBtn }
                        title="Press Me"
                    />
                </View>
                <View style={styles.buttonContainer}>
                    <Button 
                        onPress={ this._onPressBtn }
                        title="Press Me"
                        color="#841584"
                    />
                </View>
                <View style={styles.alternativeLayoutButtonContainer}>
                    <Button 
                        onPress={ this._onPressBtn }
                        title="This looks great!"
                    />
                    <Button 
                        onPress={ this._onPressBtn }
                        title="OK!"
                        color="#841584"
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    buttonContainer: {
        margin: 20
    },
    
    alternativeLayoutButtonContainer: {
        margin: 20,
        flexDirection: 'row',
        justifyContent: 'space-between'
    }
});