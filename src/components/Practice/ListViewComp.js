import React, {Component} from 'react';
import {
    View, Text, StyleSheet, FlatList, SectionList
} from 'react-native';

export default class ListViewComp extends Component {
    render() {
        return (
            // <FlatList 
            //     data={[
            //         { key : 'Shan' },
            //         { key : 'Shan1' },
            //         { key : 'Shan2' },
            //         { key : 'Shan3' },
            //         { key : 'Shan4' },
            //         { key : 'Shan5' },
            //         { key : 'Shan6' },
            //         { key : 'Shan7' },
            //         { key : 'Shan8' },
            //         { key : 'Shan9' },
            //         { key : 'Shan10' },
            //         { key : 'Shan11' },
            //         { key : 'Shan12' },
            //         { key : 'Shan13' },
            //         { key : 'Shan14' },
            //         { key : 'Shan15' },
            //     ]}

            //     renderItem={({item})=> <Text style={styles.item}>{item.key}</Text>}
            // />

            <SectionList
                sections={[
                    { title : 'Shan', data : ['Shan-data'] },
                    { title : 'Shan1', data : ['Shan1-data', 'new data', 'old data', 'post data', 'blog post'] },
                    { title : 'Shan2', data : ['Shan2-data'] },
                    { title : 'Shan3', data : ['Shan3-data'] },
                    { title : 'Shan4', data : ['Shan4-data'] },
                    { title : 'Shan5', data : ['Shan5-data'] },
                    { title : 'Shan6', data : ['Shan6-data'] },
                    { title : 'Shan7', data : ['Shan7-data'] },
                    { title : 'Shan8', data : ['Shan8-data'] },
                    { title : 'Shan9', data : ['Shan9-data'] },
                    { title : 'Shan10', data : ['Shan10-data'] },
                    { title : 'Shan11', data : ['Shan11-data'] },
                    { title : 'Shan12', data : ['Shan12-data'] },
                    { title : 'Shan13', data : ['Shan13-data'] },
                    { title : 'Shan14', data : ['Shan14-data'] },
                    { title : 'Shan15', data : ['Shan15-data'] },
                ]}
                
                renderItem={({item}) => <Text style={styles.item}>{item}</Text>}
                renderSectionHeader={({section}) => <Text style={styles.sectionHeader}>{section.title}</Text>}
                keyExtractor={(item, index) => index}
            />
        );
    }
}

const styles = StyleSheet.create({
    sectionHeader: {
        paddingTop: 2,
        paddingLeft: 10,
        paddingRight: 10,
        paddingBottom: 2,
        fontSize: 14,
        fontWeight: 'bold',
        backgroundColor: 'rgba(247,247,247,1.0)',
    },

    item: {
        padding: 10,
        fontSize: 18,
        height: 44,
    },
});