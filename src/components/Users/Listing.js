import React, {Component} from 'react';
import {View,Text,StyleSheet,FlatList,Alert,ScrollView} from 'react-native';
import HandleBack from './../../../navigation/HandleBackButtonBehaviorOnAndroid';
import axios from 'axios';

export default class Listing extends Component {
    _isMounted = false;
    
    constructor(props) {
        super(props);
        this.state = {
            isLoading : true
        };
    }

    static navigationOptions = ({navigation}) => {
        return {
            title: navigation.getParam('name'),
            headerTitleStyle: { color: '#20B2AA', width: '80%', textAlign: 'center'},
        }
    };

    componentDidMount() {
        this._isMounted = true;
        this.getUsers();
    }

    getUsers = () => {
        axios.get("https://zeeshan.botsify.com/api/users")
            .then((response) => {
                // Alert.alert(JSON.stringify(response.data));
                if (this._isMounted) {
                    this.setState({
                        users : response.data
                    });
                }
                // Alert.alert(this.state.users[0].name);
            })
            .catch((error) => console.error(error));
    }

    render() {
        return (
            // <HandleBack onBack={this.onBack}>
                <View style={styles.container}>
                    <ScrollView>
                        <FlatList 
                            data={this.state.users}
                            renderItem={({item}) => 
                                <View style={styles.listView}>
                                    <View style={styles.listNameView}><Text style={styles.listNameText}>{item.name}</Text></View>
                                    <View style={styles.listInfo}><Text style={styles.listInfoText}>i</Text></View>
                                </View>
                            }
                            keyExtractor={({id}, index) => id.toString()}
                        />
                    </ScrollView>
                </View>
            // </HandleBack>
        );
    }
}

const styles = StyleSheet.create({
    container : {
        flex: 1,
        // width: 500,
        // height: 500,
    },

    // backBtnView : {
    //     width: '100%',
    //     height: 60,
    //     justifyContent: 'space-around',
    //     alignItems: 'flex-start',
    //     alignSelf: 'flex-start',
    //     alignContent: 'center',
    //     borderBottomWidth: 3,
    // },
    // backBtn : {
    //     width: 80,
    //     height: 30,
    //     justifyContent: 'center',
    //     alignItems: 'center',
    //     alignSelf: 'flex-start',
    //     marginLeft: 25,
    //     borderTopWidth: 1,
    //     borderLeftWidth: 1,
    //     borderRightWidth: 1,
    //     borderBottomWidth: 1,
    //     paddingTop: 2,
    //     paddingLeft: 2,
    //     paddingRight: 2,
    //     paddingBottom: 2,
    // },
    listView : {
        flexDirection: 'row',
        height: 60,
        marginTop: 10,
        marginBottom : 2,
        marginLeft: 5,
        marginRight: 5,
        borderRadius: 5,
        backgroundColor: '#f0f0f0',
        justifyContent: 'space-around',
        alignItems: 'center',
    },
    listNameView : {
        marginLeft : 0,
        // width: 200,
        // height: 60,
    },
    listNameText : {
        fontSize: 22
    },
    listInfo : {
        width: 30,
        height:30,
        borderRadius: 50,
        backgroundColor: '#f6f6f6',
        paddingTop: 5,
        paddingBottom: 5,
        paddingLeft: 5,
        paddingRight: 5,
        alignItems: 'center',
        marginRight : -50,
    },
    listInfoText : {
        fontStyle: 'italic',
        color: '#333'
    }
});

/* <View style={styles.backBtnView}>
                    <View style={styles.backBtn}>
                        <Text>Back</Text>
                    </View>
                </View>*/