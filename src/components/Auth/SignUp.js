import React, {Component} from 'react';
import {View,Text,TextInput,Button,StyleSheet,TouchableOpacity} from 'react-native';

export default class SignUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fullname : '',
            email : '',
            password : '',
        };
    }

    static navigationOptions = {
        header: null,
    };

    signUp = () => {
        axios.post('https://zeeshan.botsify.com/api/account', {
            fullname : this.state.fullname,
            email : this.state.email,
            password : this.state.password,
        })
        .then(response => {
            Alert.alert('success');

        }).catch(error => {
            Alert.alert(`error ${error}`);
        });
    }

    render() {
        const {navigate} = this.props.navigation;
        return (
            <View style={styles.container}>
                <Text style={styles.text}>Sign Up with Contactor</Text>
                <TextInput style={styles.textInput} onChangeText={ (fullname) => this.setState({fullname}) } placeholder="Full Name"/>
                <TextInput style={styles.textInput} onChangeText={ (email) => this.setState({email}) } placeholder="Email"/>
                <TextInput style={styles.textInput} onChangeText={ (password) => this.setState({password}) } placeholder="Password"/>
                <TouchableOpacity onPress={ this.login }>
                    <View style={styles.textInputSubmit} >
                        <Text style={styles.touchableSignInText}>Sign Up</Text>
                    </View>
                </TouchableOpacity>

                <View style={styles.touchableSignInView}>
                    <Text style={styles.alreadyText}>Already have an account? </Text>
                    <TouchableOpacity
                            onPress={ () => navigate('Login') } >
                        <View>
                            <Text style={styles.touchableSignInText}>Sign In</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container : {
        flex : 1,
        justifyContent: 'center',
        alignItems: 'center'
    },

    text : {
        fontSize: 20,
        color: '#20B2AA',
        marginBottom: 30
    },

    textInput : {
        width: '80%',
        borderBottomWidth: 2,
        padding: 2,
        marginTop: 10,
        marginBottom: 10,
        borderBottomColor: '#20B2AA',
    },
    textInputSubmit : {
        fontSize: 20,
        borderWidth: 2,
        borderColor: '#20B2AA',
        paddingTop: 5,
        paddingBottom: 5,
        paddingLeft: 10,
        paddingRight: 10,
        marginTop: 10,
    },
    touchableSignInView : {
        marginTop: 50,
        flexDirection: 'row',
        alignItems: 'center'
    },
    alreadyText : {
        color: '#c5c5c5',
        fontSize: 16,
    },
    touchableSignInText : {
        color: '#20B2AA',
        fontSize: 20,
    }
});