import React, {Component} from 'react';
import {View,Text,TextInput,Button,StyleSheet,TouchableOpacity,Alert} from 'react-native';
import axios from 'axios';

export default class LogIn extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email : '',
            password : '',
            isLoggingIn : false,
        };
    }

    static navigationOptions = {
        header: null,
    };

    login = () => {
        axios.post('https://zeeshan.botsify.com/oauth/token', {
            grant_type : 'password',
            client_id : 2,
            client_secret : 'hzA7Xifp7PLhCzEBuq5vhxwNbFW5mmIwyrlIQKHP',
            email : this.state.email,
            password : this.state.password,
            scope: '*'
        },{
            headers : {
                'Accept' : 'application/json',
            } 
        })
        .then(response => {
            Alert.alert('success');

        }).catch(error => {
            Alert.alert(`error ${error}`);
        });

        // axios.post('https://zeeshan.botsify.com/oauth/token', 
        //         {
        //             grant_type : 'password',
        //             client_id : 2,
        //             client_secret : 'hzA7Xifp7PLhCzEBuq5vhxwNbFW5mmIwyrlIQKHP',
        //             email : this.state.email,
        //             password : this.state.password,
        //             scope: '*'
        //         }, 
        //         { 
        //             headers : {
        //                 'Accept' : 'application/json',
        //             } 
        //         }
        //     )
        //     .then((response) => {
        //         Alert.alert('success');
        //     })
        //     .catch((error) => {
        //         Alert.alert(`error ${error}`);
        //     });
    }

    render() {
        const {navigate} = this.props.navigation;
        return (
            <View style={styles.container}>
                <Text style={styles.text}>Sign Up with Contactor</Text>
                <TextInput 
                    style={styles.textInput} 
                    placeholder="Email"
                    onChangeText={ (email) => this.setState({email}) }
                />
                <TextInput 
                    style={styles.textInput} 
                    placeholder="Password"
                    onChangeText={ (password) => this.setState({password}) }
                />
                <TouchableOpacity 
                    // disabled={this.state.isLoggingIn||!this.state.username||!this.state.password}
                    onPress={ this.login }>
                    
                    <View style={styles.textInputSubmit} >
                        <Text style={styles.touchableSignInText}>Sign In</Text>
                    </View>
                </TouchableOpacity>

                <View style={styles.touchableSignInView}>
                    <Text style={styles.alreadyText}>Don't have an account? </Text>
                    <TouchableOpacity
                            onPress={ () => navigate('SignUp') } >
                        <View>
                            <Text style={styles.touchableSignInText}>Sign Up</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container : {
        flex : 1,
        justifyContent: 'center',
        alignItems: 'center'
    },

    text : {
        fontSize: 20,
        color: '#20B2AA',
        marginBottom: 30
    },

    textInput : {
        width: '80%',
        borderBottomWidth: 2,
        padding: 2,
        marginTop: 10,
        marginBottom: 10,
        borderBottomColor: '#20B2AA',
    },
    textInputSubmit : {
        fontSize: 20,
        borderWidth: 2,
        borderColor: '#20B2AA',
        paddingTop: 5,
        paddingBottom: 5,
        paddingLeft: 10,
        paddingRight: 10,
        marginTop: 10,
    },
    touchableSignInView : {
        marginTop: 50,
        flexDirection: 'row',
        alignItems: 'center'
    },
    alreadyText : {
        color: '#c5c5c5',
        fontSize: 16,
    },
    touchableSignInText : {
        color: '#20B2AA',
        fontSize: 20,
    }
});