// /**
//  * Sample React Native App
//  * https://github.com/facebook/react-native
//  *
//  * @format
//  * @flow
//  * @lint-ignore-every XPLATJSCOPYRIGHT1
//  */

// import React, {Component} from 'react';
// import {
//   StyleSheet, 
//   View
// } from 'react-native';
// import Home from './src/components/Home';
// import SignUp from './src/components/Auth/SignUp';
// import DrawerNavigator from './navigation/DrawerNavigator';

// // Platform specific description using Platform Module
// /*const instructions = Platform.select({
//   ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
//   android:
//     'Double tap R on your keyboard to reload,\n' +
//     'Shake or press menu button for dev menu',
// });*/

// export default class App extends Component {
//   render() {
//     return (
//       <SignUp />
//     );
//   }
// }

// // Style the component using StyleSheet Module
// // const styles = StyleSheet.create({
// //   container: {
// //     // flex: 1,
// //     // flexDirection:'row',
// //     // justifyContent: 'center',
// //     // alignItems: 'flex-start',
// //     // backgroundColor: '#F5FCFF',
// //     backgroundColor: '#4F6D7A',
// //   }
// // });

// /* <Banana stile={{ width:50, height:50, backgroundColor: 'violet' }} name="Pomegrannete" />
// <Banana stile={{ width:50, height:50, backgroundColor: 'indigo' }} name="Burberry" />
// <Banana stile={{ width:50, height:50, backgroundColor: 'blue' }} name="Blue Berry" />
// <Banana stile={{ width:50, height:50, backgroundColor: 'green' }} name="Water Melon" />
// <Banana stile={{ width:50, height:50, backgroundColor: 'yellow' }} name="Mango" />
// <Banana stile={{ width:50, height:50, backgroundColor: 'orange' }} name="Orange" />
// <Banana stile={{ width:50, height:50, backgroundColor: 'red' }} name="Red Grapes" /> */